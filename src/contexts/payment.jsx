import paymaya from 'paymaya-js-sdk';

function App() {
  const exampleCheckoutObject = {};
  useEffect(() => {
    paymaya.init('my-public-key', true);
    paymaya.createCheckout(exampleCheckoutObject);
  }, []);
  return (
      <div>
        <div>Test App</div>
      </div>
  );
}