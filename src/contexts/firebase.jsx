import React, {createContext, Component} from 'react';
import {Redirect} from 'react-router-dom';
import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/firestore";
import "firebase/functions";
import "firebase/analytics";

export const FirebaseContext = createContext();

const firebaseConfig = {
    apiKey: "AIzaSyDvEUEh_2nQvccxHzOA_3-054OvA4xrt-Q",
    authDomain: "lottoplayhere-510da.firebaseapp.com",
    projectId: "lottoplayhere-510da",
    storageBucket: "lottoplayhere-510da.appspot.com",
    messagingSenderId: "673805491075",
    appId: "1:673805491075:web:24e243973dac1494b5f4b6",
    measurementId: "G-3RTP359DSW"
  };

  class FirebaseContextProvider extends Component {
    constructor(props){
    super(props);
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
    this.auth = firebase.auth();
    this.store = firebase.firestore();
    this.functions = firebase.functions();
    this.analytics=firebase.analytics();
    this.fieldValue = firebase.firestore.FieldValue;
    this.timestamp = firebase.firestore.Timestamp;

    this.state= {
        status: false,
        loading: false,
        redirect: false
    }

    }

    watch = ()=>{
        this.auth.onAuthStateChanged(
        (user) => {
        if (user) {  
            this.setState( {status : true});
            localStorage.setItem("status",true);
        }
        else {
            this.setState( {status : false});
        }
         });
    }   

    signin= async () =>{
        var provider = new firebase.auth.FacebookAuthProvider();
        await firebase.auth().signInWithRedirect(provider);
        this.setState({status:true, redirect:true});
        localStorage.setItem("status",true);


    }

    signout= async() =>{
        this.setState({
            status: false,
        });
        this.auth.signOut();
        localStorage.clear();
        sessionStorage.clear();
        console.log("User signs out");
        }


    render(){
        return(
            <FirebaseContext.Provider value={{...this.state,Timestamp:this.timestamp, fieldValue:this.fieldValue,serverTimestamp:this.serverTimestamp,arrayRemove:this.arrayRemove,arrayUnion:this.arrayUnion,setUid:this.setUid,tutorset:this.tutorset,modset:this.modset,adminset:this.adminset, auth:this.auth,functions:this.functions,store: this.store, signout:this.signout,watch:this.watch,signin:this.signin}}>
                {this.props.children}
            </FirebaseContext.Provider>
        );
    }
}


export default FirebaseContextProvider;