import React, {useEffect,useContext, useState, useRef} from 'react';
import { FirebaseContext } from '../contexts/firebase';
import "../styles/play.css";
import PlayNav from './playnav';
import Sidebar from './sidebar';

function Play (){
    const fire = useContext(FirebaseContext);
    const defaultClick={A:[],B:[],C:[],D:[]};
    const defaultMode={A:6,B:6,C:6,D:6};
    const defaultRp = {A:false,B:false,C:false,D:false};
    const defaultDraw = {A:false,B:false,C:false,D:false};
    const defaultPrice = {A:false,B:false,C:false,D:false};
    const defaultRamb = {A:false,B:false,C:false,D:false};

    const [click, setClick] = useState(defaultClick);
    const [mode, setMode]=useState(defaultMode);
    const [rp,setRp]=useState(defaultRp);
    const [draw,setDraw]=useState(defaultDraw);
    const [game,setGame]=useState(42);
    const [digitGame,setDigitGame]=useState(0);
    const [price,setPrice]=useState(defaultPrice);
    const [ramb,setRamb]=useState(defaultRamb);
    const [drop,setDrop]=useState(null);
    const [board,setBoard]=useState([]);
    const [digit, setDigit] = useState(false);

    const wrapperRef = useRef(null);

    function ResetBoards(){
        setClick(defaultClick);
        setMode(defaultMode);
        setRp(defaultRp);
        setDraw(defaultDraw);
        setRamb(defaultRamb);
        setPrice(defaultPrice);
        setDrop(null);
    }

/*     useEffect(()=>{
        if (fire.status){
            setLoading(false);
        }
    },[fire.status]) */

    useEffect(() => {
        window.addEventListener("mousedown", handleClickOutside);
        return () => {
          window.removeEventListener("mousedown", handleClickOutside);
        };
      });

    const handleClickOutside = event => {
        const { current: wrap } = wrapperRef;
        if (wrap && !wrap.contains(event.target)) {
          setDrop(false);
        }
      };

    function shuffle(array) {
        var i = array.length,
            j = 0,
            temp;
    
        while (i--) {
    
            j = Math.floor(Math.random() * (i+1));
    
            // swap randomly chosen element with current element
            temp = array[i];
            array[i] = array[j];
            array[j] = temp;
    
        }
    
        return array;
    }

    useEffect(()=>{
        switch (game) {
            case 42:
                setBoard("6/42");
                break;
            case 45:
                setBoard("6/45");
                break;
            case 49:
                setBoard("6/49");
                break;
            case 55:
                setBoard("6/55");
                break;
            case 58:
                setBoard("6/58");
                break;
            case 31:
                setBoard( "EZ2");
                break;
            case 9:
                switch (digitGame){
                    case 3:
                        setBoard("3D");
                        break;
                    case 4:
                        setBoard("4D");
                        break;
                    case 6:
                        setBoard("6D");
                        break;
                }
                break
            default:
                console.log(`Sorry, we are out of bananas.`);
            }
    },[game,digitGame])

    //lotto games
    const numArray = Array.from({length: game}, (_, i) => i + 1); 
    const modeArray = ["5R","Sys 7","Sys 8","Sys 9","Sys 10","Sys 11","Sys 12"];
    const drawArray = ["1 Draw","2 Draws","3 Draws","6 Draws"]

    const card = letter=>[...numArray,"LP"].map((element,i)=>
        <div className="play-num" role="button" onClick={()=>
            {
            //lucky pick
            if(element==="LP"){
                //remove lp
                if(rp[letter]===true){
                    setClick({...click,[letter]:[]});
                    setRp({...rp,[letter]:false});
                }
                //perform lp
                else{
                    setClick({...click,[letter]:[...shuffle(numArray).slice(0,mode[letter]),game+1]});
                    setRp({...rp,[letter]:true});
                }
            }
            //normal play
            else{
                setRp({...rp,[letter]:false});

                let y= {};
                if(click[letter].includes(i+1)){
                    y=click[letter].filter(e=>e!==i+1);
                }
                //add number
                else if(click[letter].length<mode[letter]){
                    y=[...click[letter],i+1];
                }
                //do nothing
                else{
                    y=click[letter];
                }

                setClick({...click,[letter]:y})
            }
            }
            } key={i}>
            <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
            </svg>
            </div>
                  {click[letter].includes(i+1)?<div><div className="vertical-line"></div>{String(element).padStart(2,"0")}</div>:String(element).padStart(2,"0")}
            <div className="play-num-bracket-bottom">
            <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
            </svg>
            </div>
        </div>
    )

    const modes = letter=>modeArray.map((element,i)=>
        <div className="play-num" onClick={
            ()=>{
                setClick({...click,[letter]:[]});
                if(i===0&&mode[letter]!==5){
                    setMode({...mode,[letter]:5});
                    setRp({...rp,[letter]:false});
                }else if (mode[letter]!==i+6){
                    setMode({...mode,[letter]:i+6});
                    setRp({...rp,[letter]:false});
                }else{
                    setMode({...mode,[letter]:6});
                    setRp({...rp,[letter]:false});
                }
            }
            } key={i}>
            <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
            </svg>
            </div>
                {(i===0&&mode[letter]===5)?<div><div className="vertical-line"></div>{element}</div>:(i!==0&&i+6===mode[letter])?<div><div className="vertical-line"></div>{element}</div>:element}
            <div className="play-num-bracket-bottom">
            <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
            </svg>
            </div>
        </div>    
    )


    const draws = letter=>drawArray.map((element,i)=>
    <div className="play-num no-center" onClick={
        ()=>{ 
            if(draw[letter]===i){
                setDraw({...draw,[letter]:null});
                }
            else {
                setDraw({...draw,[letter]:i});
            }
            }
        } 
        key={i}>
        <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
        </svg>
        </div>
            {<div>{draw[letter]===i?<div className="vertical-line"></div>:""}{element}</div>}
        <div className="play-num-bracket-bottom">
        <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
        </svg>
        </div>
    </div>    
)

    const playCards=["A","B","C","D"].map((element,i)=>
        <div className="play-card">
            <div className="play-card-label">
                {element}
            </div>
                {card(element)}
            <div className="play-card-system">
            <div className="play-card-system-label">System Play</div>
                {modes(element)}
            </div>
            <div className="play-card-system">
            <div className="play-card-system-label">No. of Draws</div>
                {draws(element)}
            </div>
        </div>
        )
    
    //digit games
    const priceArray = [10,20,30,50,80,100,150,200,300,500];
    //prices
    const prices = (letter)=>priceArray.map((element,i)=>
        <div className="play-num-ez2" onClick={
            ()=>setPrice({...price,[letter]:element})
            }>
            <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
            </svg>
            </div>
                {price[letter]===element ?<div><div className="vertical-line"></div>{String(element).padStart(2,"0")}</div>:String(element).padStart(2,"0")}
            <div className="play-num-bracket-bottom">
            <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
            </svg>
            </div>
        </div>
    )
    
    //numbers init
    const ez2 = (letter,number=0)=>[...numArray].map((element,i)=>
        <>
        <div key={i} className="play-num-ez2" onClick={()=>
            {

            //normal play

            setRp({...rp,[letter]:false});

            let y= click[letter];
            if(click[letter][number]===i+1){
                y[number]=0;
                setPrice({...price,[letter]:0});
            }
            //add number
            else if(!(click[letter][0]&&click[letter][1])){
                y[number]=i+1;
                if(game===31&&click[letter][0]&&click[letter][1]){
                    setPrice({...price,[letter]:price[letter]||10});
                }
            }
            
            //do nothing
            else{
                y=click[letter];
            }

            setClick({...click,[letter]:y});
            
            }
            }>
            <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
            </svg>
            </div>
                {click[letter][number]===i+1 ?<div><div className="vertical-line"></div>{String(element).padStart(2,"0")}</div>:String(element).padStart(2,"0")}
            <div className="play-num-bracket-bottom">
            <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
            </svg>
            </div>
        </div>
        </>
    );

    const xd = (letter,number=0)=>[...numArray].map((element,i)=>
    <>
    <div onClick={()=>
            {         
            //normal play
            setRp({...rp,[letter]:false});
            console.log("click",click,number,click[letter].length,click[letter].length);
            let y= click[letter];
            if(click[letter][number]===i+1){
                y[number]="";
                setPrice({...price,[letter]:0});
            }
            //add number
            else if(!(click[letter].join("").length===digitGame)){
                y[number]=i+1;
                if((click[letter].join("").length===digitGame)){
                    setPrice({...price,[letter]:price[letter]||10});
                }
                 if (ramb[letter]==="ROLL1"||ramb[letter]==="ROLL4"){
                    if (ramb[letter]==="ROLL1"){
                        y[0]="";
                    }
                    else if (ramb[letter]==="ROLL4"){
                        y[3]="";
                    }
                    if((click[letter].join("").length===digitGame-1)){
                        setPrice({...price,[letter]:price[letter]||10});
                    }
                } 
            }


            //do nothing
            else{
                y=click[letter];
            }

            setClick({...click,[letter]:y});
            
            }
            }key={i} className="play-num-ez2">
        <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
        </svg>
        </div>
            {click[letter][number]===i+1 ?<div><div className="vertical-line"></div>{String(element).padStart(2,"0")}</div>:String(element).padStart(2,"0")}
        <div className="play-num-bracket-bottom">
        <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
        </svg>
        </div>
    </div>
    </>
    );

    //additional options
    const digitOptions = (letter)=>
        <div className={`play-card-ez2-wrapper2 ${digitGame===2?"":"no-flex"}`}>
        <div className={`play-card-ez2-options ${digitGame===2?"":"no-flex"}`}>
            <div className="play-card-system-label">Options</div>
            <div onClick={()=>{
                    //remove lp
                    if(rp[letter]===true){
                        setClick({...click,[letter]:[]});
                        setRp({...rp,[letter]:false});
                        setPrice({...price,[letter]:0});
                    }
                    //perform lp
                    else if (rp[letter]===false && (ramb[letter]==="ROLL1"||ramb[letter]==="ROLL4")){
                        ramb[letter]==="ROLL1"?
                        setClick({...click,[letter]:["",...shuffle(numArray).slice(0,digitGame-1)]}):
                        setClick({...click,[letter]:[...shuffle(numArray).slice(0,digitGame-1),""]})
                        setRp({...rp,[letter]:true});
                        setPrice({...price,[letter]:price[letter]||10});
                    }
                    else{
                        setClick({...click,[letter]:[...shuffle(numArray).slice(0,digitGame)]});
                        setRp({...rp,[letter]:true});
                        setPrice({...price,[letter]:price[letter]||10});
                    }
            }}className="play-num-ez2">
                <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
                </svg>
                </div>
                {<div>{rp[letter]===true?<div className="vertical-line"></div>:""}LP</div>}
                <div className="play-num-bracket-bottom">
                <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
                </svg>
                </div>
            </div>
            {digitGame===2||digitGame===3?//EZ2, Suertres Rambolito
            <div onClick={()=>{
                    //remove ramb
                    if(ramb[letter]===true){
                        setRamb({...ramb,[letter]:false});
                    }
                    //add ramb
                    else{
                        setRamb({...ramb,[letter]:true});
                    }
            }}className="play-num-ez2">
                <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
                </svg>
                </div>
                {<div>{ramb[letter]===true?<div className="vertical-line"></div>:""}Rambolito</div>}
                <div className="play-num-bracket-bottom">
                <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
                </svg>
                </div>
            </div>:digitGame===4?//4D Rambolito
                        <>
                        <div onClick={()=>{
                            //remove roll1
                            if(ramb[letter]==="ROLL1"){
                                setRamb({...ramb,[letter]:false});
                                setRp({...rp,[letter]:false});
                                setClick({...click,[letter]:[]});
                            }
                            //add roll1
                            else{
                                setRamb({...ramb,[letter]:"ROLL1"});
                                setRp({...rp,[letter]:false});
                                setClick({...click,[letter]:[]});
                            }
                    }}className="play-num-ez2">
                        <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
                        </svg>
                        </div>
                        {<div>{ramb[letter]==="ROLL1"?<div className="vertical-line"></div>:""}ROLL 1</div>}
                        <div className="play-num-bracket-bottom">
                        <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
                        </svg>
                        </div>
                    </div>
                    <div onClick={()=>{
                        //remove roll4
                        if(ramb[letter]==="ROLL4"){
                            setRamb({...ramb,[letter]:false});
                            setRp({...rp,[letter]:false});
                            setClick({...click,[letter]:[]});
                        }
                        //add roll4
                        else{
                            setRamb({...ramb,[letter]:"ROLL4"});
                            setRp({...rp,[letter]:false});
                            setClick({...click,[letter]:[]});
                        }
                }}className="play-num-ez2">
                    <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
                    </svg>
                    </div>
                    {<div>{ramb[letter]==="ROLL4"?<div className="vertical-line"></div>:""}ROLL 4</div>}
                    <div className="play-num-bracket-bottom">
                    <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
                    </svg>
                    </div>
                </div>
                </>
                    :""
            }
        </div>
        <div className={`play-card-system ${digitGame===2?"":"no-flex"}`}>
            <div className="play-card-system-label">No. of Draws</div>
                {draws(letter)}
        </div>
    </div>

    //card init
    const ez2Card = (element)=>
    <>
                <div className="play-card-ez2-wrapper1">
                <div className="play-card-label">
                    {element}
                </div>
                <div className="play-card-ez2-numbers">
                    {ez2(element,0)}
                </div>
                <div className="divider"></div>
                <div className="play-card-ez2-numbers">
                    {ez2(element,1)}
                </div>
                <div className="divider"></div>
                <div className="play-card-prices">
                    <div className="play-card-system-label">AMT</div>
                    {prices(element)}
                </div>
            </div>
            {digitOptions(element)}
    </>

    const digitCard = (digitGame,element)=>
    <>
        <div className="play-card-label">
            {element}
        </div>
        {Array.from({length: digitGame}, (v, i) => i).map((e,i)=>
        <div className="play-card-digit-col">
            <div>
                {xd(element,i)}
            </div>
        </div>
        )}
        <div className="divider">
        </div>
        <div className="play-card-system-label digit-label">
        AMT
        {prices(element)}
        </div>
        <div className="divider">
        </div>
        <div className="test">
        {digitOptions(element)}
        </div>
    </>
    
    const digitCards=["A","B","C","D"].map((element,i)=>
        <div className={`play-card flex-column ${digitGame===2?"negate-max-content":""}`} key={i}>
            {digitGame===2?ez2Card(element):digitCard(digitGame,element)}
        </div>
    );



    return(
        <div className="play-container">
        <PlayNav/>
        <div className="play-wrapper">
            <Sidebar reset={(value)=>value?ResetBoards():""} mode={mode} draw={draw} lp={rp} nums={click} price={price} game={game} menu={Object.values(click).every(e=>e.length===0||false)?true:false}/>
                <div className="play-area">
                    <div className="play-game">
                        <div className="play-game-choose">
                            {board} <span onClick={()=>setDrop(true)}><svg width="36" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#F8F9FF"/>
                            </svg></span>
                            <div style={{fontSize:18}}>
                            Next Draw: Today, 9PM
                            </div>
                        </div>
                        <div ref={wrapperRef} className="play-game-menu" style={{display:drop?"grid":"none"}}>
                            <div onClick={()=>{setDigit(false);setGame(42);ResetBoards();}} className="play-game-menu-item">6/42</div>
                            <div onClick={()=>{setDigit(false);setGame(45);ResetBoards();}} className="play-game-menu-item">6/45</div>
                            <div onClick={()=>{setDigit(false);setGame(49);ResetBoards();}} className="play-game-menu-item">6/49</div>
                            <div onClick={()=>{setDigit(false);setGame(55);ResetBoards();}} className="play-game-menu-item">6/55</div>
                            <div onClick={()=>{setDigit(false);setGame(58);ResetBoards();}} className="play-game-menu-item">6/58</div>
                            <div onClick={()=>{setDigit(true);setGame(31);setDigitGame(2);ResetBoards();}}className="play-game-menu-item">EZ2</div>
                            <div onClick={()=>{setDigit(true);setGame(9);setDigitGame(3);ResetBoards();}} className="play-game-menu-item">3D</div>
                            <div onClick={()=>{setDigit(true);setGame(9);setDigitGame(4);ResetBoards();}}className="play-game-menu-item">4D</div>
                            <div onClick={()=>{setDigit(true);setGame(9);setDigitGame(6);ResetBoards();}} className="play-game-menu-item">6D</div>
                        </div>
                        <div className="play-game-jackpot">
                            <div className="play-game-jackpot-prize">
                                P 56,000,212.23
                            </div>  
                            <div className="play-game-jackpot-label">
                                Jackpot Prize
                            </div>
                        </div>
                    </div>
                    <div className="play-card-container">
                    {digit?digitCards:playCards}
                    </div>
                </div>
        </div> 
            </div>
    )
}

export default Play;


/**
 *         <div className="play-container">
            <PlayNav/>
            <div className="play-wrapper">
                <Sidebar/>
                </div> 
                 </div>


                     async function checkout (){
        const res = await fetch(
            "https://pg-sandbox.paymaya.com/checkout/v1/checkouts",{
                method: "POST",
                headers: {
                    "Content-Type":"application/json",
                    "Authorization":"Basic cGstWjBPU3pMdkljT0kyVUl2RGhkVEdWVmZSU1NlaUdTdG5jZXF3VUU3bjBBaA=="
                },
                body: {          
                    "totalAmount": {
                      "value": "100.00",
                      "currency": "PHP",
                      "details": {
                        "discount": "0.00",
                        "serviceCharge": "0.00",
                        "shippingFee": "0.00",
                        "tax": "0.00",
                        "subtotal": "100.00"
                      }
                    },
                    "buyer": {
                      "firstName": "John",
                      "middleName": "Paul",
                      "lastName": "Doe",
                      "birthday": "1995-10-24",
                      "customerSince": "1995-10-24",
                      "sex": "M",
                      "contact": {
                        "phone": "+639181008888",
                        "email": "merchant@merchantsite.com"
                      },
                      "shippingAddress": {
                        "firstName": "John",
                        "middleName": "Paul",
                        "lastName": "Doe",
                        "phone": "+639181008888",
                        "email": "merchant@merchantsite.com",
                        "line1": "6F Launchpad",
                        "line2": "Reliance Street",
                        "city": "Mandaluyong City",
                        "state": "Metro Manila",
                        "zipCode": "1552",
                        "countryCode": "PH",
                        "shippingType": "ST" 
                      },
                      "billingAddress": {
                        "line1": "6F Launchpad",
                        "line2": "Reliance Street",
                        "city": "Mandaluyong City",
                        "state": "Metro Manila",
                        "zipCode": "1552",
                        "countryCode": "PH"
                      }
                    },
                    "items": [
                      {
                        "name": "Canvas Slip Ons",
                        "quantity": "1",
                        "code": "CVG-096732",
                        "description": "Shoes",
                        "amount": {
                          "value": "100.00",
                          "details": {
                            "discount": "0.00",
                            "serviceCharge": "0.00",
                            "shippingFee": "0.00",
                            "tax": "0.00",
                            "subtotal": "100.00"
                          }
                        },
                        "totalAmount": {
                          "value": "100.00",
                          "details": {
                            "discount": "0.00",
                            "serviceCharge": "0.00",
                            "shippingFee": "0.00",
                            "tax": "0.00",
                            "subtotal": "100.00"
                          }
                        }
                      }
                    ],
                    "redirectUrl": {
                        "success": "https://www.merchantsite.com/success",
                        "failure": "https://www.merchantsite.com/failure",
                        "cancel": "https://www.merchantsite.com/cancel"
                    },
                    "requestReferenceNumber": "1551191039",
                    "metadata": {}
                }
            }
        )
        console.log(res.json());
    }
 * 
    {/*             <div className="play-card-ez2-wrapper2">
                <div className="play-card-ez2-options">
                    <div className="play-card-system-label">Options</div>
                    <div onClick={()=>{
                            //remove lp
                            if(rp[element]===true){
                                setClick({...click,[element]:[]});
                                setRp({...rp,[element]:false});
                                setPrice({...price,[element]:0});
                            }
                            //perform lp
                            else{
                                setClick({...click,[element]:[...shuffle(numArray).slice(0,2)]});
                                setRp({...rp,[element]:true});
                                setPrice({...price,[element]:price[element]||10});
                            }
                    }}className="play-num-ez2">
                        <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
                        </svg>
                        </div>
                        {<div>{rp[element]===true?<div className="vertical-line"></div>:""}LP</div>}
                        <div className="play-num-bracket-bottom">
                        <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
                        </svg>
                        </div>
                    </div>
                    <div onClick={()=>{
                            //remove ramb
                            if(ramb[element]===true){
                                setRamb({...ramb,[element]:false});
                            }
                            //add ramb
                            else{
                                setRamb({...ramb,[element]:true});
                            }
                    }}className="play-num-ez2">
                        <div className="play-num-bracket-top"><svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.3583 0L0.5 4.8583L1.6417 6L5.3583 2.2915L9.0749 6L10.2166 4.8583L5.3583 0Z" fill="#34373A"/>
                        </svg>
                        </div>
                        {<div>{ramb[element]===true?<div className="vertical-line"></div>:""}Rambolito</div>}
                        <div className="play-num-bracket-bottom">
                        <svg width="11" height="6" viewBox="0 0 11 6" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M5.35825 6L10.2166 1.1417L9.07485 9.98106e-08L5.35825 3.7085L1.64165 7.49642e-07L0.499953 1.1417L5.35825 6Z" fill="#34373A"/>
                        </svg>
                        </div>
                    </div>
                </div>
                <div className="play-card-system">
                    <div className="play-card-system-label">No. of Draws</div>
                        {draws(element)}
                </div>
            </div>
        */