import React, {useEffect,useContext, useState} from 'react';
import {Switch,Route,Redirect} from 'react-router-dom';
import Landing from './landing.jsx';
import Play from './play.jsx';
import Bets from './bets.jsx';
import PlayMain from './playmain.jsx';
import ScrollToTop from './scripts/ScrollToTop.js';
import { FirebaseContext } from '../contexts/firebase.jsx';

function Main(){
    const fire = useContext(FirebaseContext);

    useEffect(()=>{
        fire.watch();
    },[])
    
    return(
/*         <ScrollToTop> */
            <Switch>
                <Route exact path="/" component={Landing}/>
                <Route exact path="/bets" component={Bets}/>
                <Route exact path="/play" component={Play}/>
            </Switch>
/*         </ScrollToTop> */
    )
}

function PrivateRoute({ children, ...rest }) {
    const fire = useContext(FirebaseContext);

    return (
      <Route
        {...rest}
        render={({ location }) =>
          //fire.status 
          localStorage.getItem("status")? (
            children
          ) : (
            <Redirect
              to={{
                pathname: "/",
                state: { from: location }
              }}
            />
          )
        }
      />
    );
  }

export default Main;