import '../styles/sidebar.css';
import '../styles/play.css';

import Play from './play';
import Sidebar from './sidebar';
import PlayNav from './playnav';

function PlayMain(){
    
    return(
        <div className="play-container">
            <PlayNav/>
            <div className="play-wrapper">
                <Sidebar/>
                <Play/>
            </div>
        </div>
    )
}

export default PlayMain;