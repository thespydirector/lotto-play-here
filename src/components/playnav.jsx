//For play area navbar

import '../styles/playnav.css';

function PlayNav(){
    let date = Date.now();
    let dateObj = new Date(date);

    return(
        <div className="play-nav">
            <div className="play-nav-logo">
                LOTTO PLAY HERE
            </div>
            <div className="play-nav-info">
                <div className="play-nav-info-status">
                    <span>
                        <svg width="23" height="16" viewBox="0 0 23 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd" d="M0 4.68346L2.05753 6.74099C7.1705 1.62802 15.4623 1.62802 20.5753 6.74099L22.6328 4.68346C16.3882 -1.56115 6.2549 -1.56115 0 4.68346ZM8.23006 12.9137L11.3164 16L14.4027 12.9137C12.7052 11.206 9.93781 11.206 8.23006 12.9137ZM6.17256 10.856L4.11503 8.79848C8.09635 4.82744 14.5467 4.82744 18.5177 8.79848L16.4602 10.856C13.6208 8.01661 9.01195 8.01661 6.17256 10.856Z" fill="#34373A"/>
                        </svg>     System Online
                    </span>
                </div>
                <div className="play-nav-info-date"> 
                    {dateObj.toDateString()}
                </div>
                <div className="play-nav-info-time">
                    {dateObj.toTimeString()}
                </div>
            </div>
        </div>
    )
}

export default PlayNav;