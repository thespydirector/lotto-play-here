import './App.css';
import Main from './components/main.jsx';
import FirebaseContextProvider from './contexts/firebase.jsx';
import {BrowserRouter} from 'react-router-dom';


function App() {
  return (
    <FirebaseContextProvider>
      <BrowserRouter>
          <Main/>
      </BrowserRouter>
    </FirebaseContextProvider>

  );
}

export default App;
